﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Szófelhő
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private List<string> vers=new List<string>() {
                "Six little ducks that I once knew," ,
                "Fat ones, skinny ones, fair ones too!",
                "But the one little duck with the feather on her back,",
                "She led the others with a quack quack quack.",
                "A quack quack quack. A quack quack quack.",
                "She led the others with a quack quack quack.",
                "",
                "Down to the river they would go,",
                "A wibble wobble, wibble wobble, to and fro.",
                "But the one little duck with the feather on her back,",
                "She led the others with a quack quack quack.",
                "A quack quack quack. A quack quack quack.",
                "She led the others with a quack quack quack.",
                "",
                "Home from the river they would come,",
                "A wibble wobble, wibble wobble, ho hum hum.",
                "But the one little duck with the feather on her back,",
                 "She led the others with a quack quack quack.",
                "A quack quack quack. A quack quack quack.",
                "She led the others with a quack quack quack.",
                "A quack quack quack quack- QUACK QUACK!"

            };
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            szöveg.Text = "";
            
            foreach (string sor in vers)
                szöveg.Text += sor + "\n";
            feldolgoz();
        }
        int maxelőfordulás;
        int határritka;
        int határgyakori;
        Dictionary<string, int> szavakelőfordulása = new Dictionary<string, int>();
        Random r = new Random();
        private void Feltölt(ref Dictionary<string, int> szavakelőfordulása)
        {
            
            List<string> nemábrázolandó = new List<string>() { "a", "the", "this", "that", "and", "or","ones","once","she","to","fro","from" };
            string s = "";
            foreach  (string sor in vers)
                s += sor.ToLower();
            s = szöveg.Text.Replace('.', ' ').Replace('!', ' ').Replace('?', ' ').Replace(',',' ');
            string[] szavak = s.Split(new char[] { ' ' });
            maxelőfordulás = 1;
            string kisbetűsszó;
            foreach (string szó in szavak)
            {
                kisbetűsszó = szó.Trim().ToLower();
                 //csak kisbetűs változatot nézünk
                if (!nemábrázolandó.Contains(kisbetűsszó) && kisbetűsszó.Length>2)
                {
                    if (!szavakelőfordulása.Keys.Contains(kisbetűsszó))
                        szavakelőfordulása[kisbetűsszó] = 1;  //szó első előfordulása
                    else
                    {
                        szavakelőfordulása[kisbetűsszó]++;
                        if (maxelőfordulás < szavakelőfordulása[kisbetűsszó]) maxelőfordulás = szavakelőfordulása[kisbetűsszó];
                    }
                }
            }
        }
        private Border egyszó(string szó)
        {
            
            Border keret=new Border();
            TextBlock szövegdoboz=new TextBlock();
            RotateTransform forgat = new RotateTransform();
            switch (r.Next(4)) //véletlen szín az íráshoz
            {
                case 0:
                    szövegdoboz.Foreground=Brushes.Red;
                    break;
                case 1:
                    szövegdoboz.Foreground = Brushes.Green;
                    break;
                case 2:
                    szövegdoboz.Foreground = Brushes.Blue;
                    break;
                case 3:
                    szövegdoboz.Foreground = Brushes.Black;
                    break;
                
            }
            szövegdoboz.Text = szó+" ";
           
            keret.Child = szövegdoboz;
           
            if (szavakelőfordulása[szó] <= határritka)
            {
                szövegdoboz.FontSize= 12;
                szövegdoboz.FontWeight = FontWeights.Normal;
                szövegdoboz.Margin = new Thickness(10, 10, 10, 10);
                if (r.Next(100) < 30)
                {
                    forgat.Angle = 90;
                    keret.RenderTransform = forgat;
                }
            }
            if (szavakelőfordulása[szó] > határritka && szavakelőfordulása[szó]<=határgyakori)
            {
                szövegdoboz.FontSize= 24;
                szövegdoboz.FontWeight = FontWeights.Bold;
                if (r.Next(100) < 20)
                {
                    forgat.Angle = 90;
                    keret.RenderTransform = forgat;
                }                              
            }
            if (szavakelőfordulása[szó] > határgyakori)
            {
                szövegdoboz.FontSize= 50;
                szövegdoboz.FontWeight = FontWeights.ExtraBold;
            }
            return keret;
        }

        private void feldolgoz()
        {
            szófelhő.Children.Clear();
            Feltölt(ref szavakelőfordulása);
            
            határritka =  1;
            határgyakori = 5;
            foreach (string szó in szavakelőfordulása.Keys)
            {
                
                   szófelhő.Children.Add(egyszó(szó));
            }
            
        }
    }
}
